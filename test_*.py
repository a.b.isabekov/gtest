import pytest
import requests
import requests_mock
import json
from io import StringIO
import pandas as pd
from main import OzonAPI, WildberriesAPI, MarketplaceTransformer


@pytest.fixture
def ozon_api():
    return OzonAPI('test_client_id', 'test_api_key')

@pytest.fixture
def wildberries_api():
    return WildberriesAPI('test_api_key')

@pytest.fixture
def transformer():
    return MarketplaceTransformer('test_client_id', 'test_api_key')

def test_get_attributes(ozon_api, requests_mock):
    attributes_data = {
        "result": [
            {
                "id": 85,
                "name": "Бренд",
                "description": "Укажите наименование бренда"
            }
        ]
    }
    requests_mock.post('https://api-seller.ozon.ru/v1/description-category/attribute', json=attributes_data)
    attributes = ozon_api.get_attributes(92130764, 96836)
    assert attributes == attributes_data

def test_transformer_load_csv(transformer):
    csv_data = """Категория,Товар
3D-принтеры,Принтер XYZ
"""
    csv_file = StringIO(csv_data)
    data = transformer.load_csv(csv_file)
    assert isinstance(data, pd.DataFrame)
    assert data.iloc[0]['Категория'] == '3D-принтеры'